from django.shortcuts import render
from django.shortcuts import HttpResponse
import random

def index(request):
    # randomlistx1 = random.sample(range(1, 10), 5)
    # randomlistx2 = random.sample(range(1, 10), 5)
    randomlistx1 = [1, 2, 3, 4, 5]
    randomlistx2 = [1, 2, 3, 4, 5]
    randomlisty1 = random.sample(range(10, 30), 5)
    randomlisty2 = random.sample(range(10, 30), 5)
    data = {'x1': randomlistx1,
            'x2': randomlistx2,
            'y1': randomlisty1,
            'y2': randomlisty2,
            }
    return render(request, 'plot_graph/index.html', data)


def ajax(request):
    return render(request, 'plot_graph/ajax.html')
    

def load(request):
    randomlistx1 = [1, 2, 3, 4, 5]
    randomlistx2 = [1, 2, 3, 4, 5]
    randomlistx3 = [1, 2, 3, 4, 5]
    randomlistx4 = [1, 2, 3, 4, 5]
    randomlistx5 = [1, 2, 3, 4, 5]
    randomlistx6 = [1, 2, 3, 4, 5]
    randomlisty1 = random.sample(range(10, 30), 5)
    randomlisty2 = random.sample(range(10, 30), 5)
    randomlisty3 = random.sample(range(10, 30), 5)
    randomlisty4 = random.sample(range(10, 30), 5)
    randomlisty5 = random.sample(range(10, 30), 5)
    randomlisty6 = random.sample(range(10, 30), 5)
    data = {'x1': randomlistx1,
            'x2': randomlistx2,
            'x3': randomlistx3,
            'x4': randomlistx4,
            'x5': randomlistx5,
            'x6': randomlistx6,
            'y1': randomlisty1,
            'y2': randomlisty2,
            'y3': randomlisty3,
            'y4': randomlisty4,
            'y5': randomlisty5,
            'y6': randomlisty6,
            }
    return JsonResponse(data)



