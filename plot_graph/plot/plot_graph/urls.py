from django.urls import path
from . import views


urlpatterns = [
    path('', views.index),
    path('ajax/', views.ajax),
    path('load/', views.load),
]
